import { Component } from '@angular/core';
import { LoginService } from './services/login-service/login.service';

@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of Heroes';

  constructor(private loginService: LoginService) {}

}
