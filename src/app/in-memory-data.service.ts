import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const heroes = [
            { id: 11, name: 'Mrs. Wet Pussy' },
            { id: 12, name: 'Mr. Dick' },
            { id: 13, name: 'Dr. 69' },
            { id: 14, name: 'Naruto' },
            { id: 15, name: 'Sasuke' },
            { id: 16, name: 'Mikasa' },
            { id: 17, name: 'Levi' }
        ];
        return { heroes };
    }
}
