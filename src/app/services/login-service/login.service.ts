import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../../login/user';
import { Http } from '@angular/http';
import { tap, catchError } from 'rxjs/operators';
import { MessageService } from '../message-service/message.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loggedIn: boolean;
  private authUrl = 'localhost:3000/api/auth';

  constructor(private messageService: MessageService, private http: HttpClient) {
    this.loggedIn = false;
  }

  isLogged(): boolean {
    return this.loggedIn;
  }

  login(user: User, password: string): Observable<string> {
    const response =  this.http.post<string>(`${this.authUrl}/login`, { email: user.email, password: password }).pipe(
      tap(_ => this.messageService.add(`Trying to login the user <${user.email}>`)),
      catchError(this.handleError<string>('login'))
    );

    this.loggedIn = true;
    return response;
  }

  logout(): void {
    this.loggedIn = false;
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.messageService.add(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

}
