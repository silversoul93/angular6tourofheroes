import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { Hero } from './hero';
import { HeroService } from '../services/hero-service/hero.service';
import { MessageService } from '../services/message-service/message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit, OnChanges, OnDestroy {
  heroes: Hero[];

  constructor(private heroService: HeroService, private messageService: MessageService) {
    console.log('constructor');
  }

  ngOnInit() {
    this.messageService.add('Heroes selected');
    this.getHeroes();
  }

  ngOnChanges() {
    this.messageService.add('Heroes page changed!');
  }

  ngOnDestroy() {
    this.messageService.add('Ouch, you destroy me!');
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => this.heroes.push(hero));
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero)
    .subscribe();
  }

}
