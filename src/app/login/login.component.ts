import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { Router } from '@angular/router';
import { LoginService } from '../services/login-service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit() {
  }

  tryLogin(email: string, password: string) {
    this.loginService.login({ email } as User, password).subscribe(
      token => {
        if (token) {
          this.router.navigate(['/dashboard']);
        } else {
          alert('Login failed!');
        }
      }
    );
  }

}
